import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/bloc/favorite_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_main_button.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:majoo_jonatan/model/people_model.dart';


class DetailPeople extends StatefulWidget {
  PeopleModel people;
  String status;

  DetailPeople({
    required this.people,
    required this.status,
    Key? key
  }) : super(key: key);

  @override
  _DetailPeopleState createState() => _DetailPeopleState();
}

class _DetailPeopleState extends State<DetailPeople> {

  final List<String> imgList = [
    'https://images.unsplash.com/photo-1521249692263-e0659c60326e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MXwxfDB8MXxhbGx8fHx8fHx8fA&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit',
    'https://images.unsplash.com/photo-1494256997604-768d1f608cac?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MXwxfDB8MXxhbGx8fHx8fHx8fA&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit',
    'https://images.unsplash.com/photo-1570824104150-ab71ef55761c?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTYyODk0MzEwMg&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080'
  ];

  @override
  Widget build(BuildContext context) {
    FavoriteBloc fBloc = BlocProvider.of<FavoriteBloc>(context);
    return SafeArea(
        child: Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                expandedHeight: 320.0,
                floating: true,
                elevation: 0,
                backgroundColor: Colors.redAccent,
                actionsIconTheme: const IconThemeData(opacity: 0.0),
                flexibleSpace: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: CarouselSlider(
                        options: CarouselOptions(
                          autoPlay: false,
                          viewportFraction: 1,
                          aspectRatio: 1,
                          enlargeCenterPage: false,
                        ),
                        items: imgList
                            .map(
                              (item) => Container(
                            height: 320,
                            color: Colors.white,
                            child: Center(
                              child: ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                    bottomLeft: Radius.circular(24),
                                      bottomRight: Radius.circular(24)),
                                  child: CachedNetworkImage(
                                    placeholder: (context, url) => const Center(
                                      child: SizedBox(
                                        width: 35.0,
                                        height: 35.0,
                                        child: CupertinoActivityIndicator(),
                                      ),
                                    ),
                                    errorWidget: (context, url, error) => Icon(Icons.error, color: Colors.black87.withOpacity(0.5),),
                                    fadeInDuration: const Duration(milliseconds: 0),
                                    fadeOutDuration: const Duration(milliseconds: 0),
                                    imageUrl: item,
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 320.0,)),
                            ),
                          ),
                        )
                            .toList(),
                      ),
                    ),
                  ],
                ),
              ),
              SliverPadding(
                padding: const EdgeInsets.only(left: 16, right: 15, top: 12),
                sliver: SliverToBoxAdapter(
                  child: Wrap(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(widget.people.name.toString(), style: const TextStyle(fontSize: 22, color: Colors.black87, fontWeight: FontWeight.w700),),
                              const SizedBox(
                                width: 4,
                              ),
                              Icon(widget.people.gender.toString() == "male" ? Ionicons.male_outline : widget.people.gender.toString() == "female" ? Ionicons.female_outline : Ionicons.transgender_outline)
                            ],
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(widget.people.birthYear.toString(), style: const TextStyle(fontSize: 12),),
                              const SizedBox(
                                width: 4,
                              ),
                              const Icon(
                                Ionicons.people_outline, size: 20,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 110,
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                                  color: Colors.blueAccent.withOpacity(0.1),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    const Spacer(),
                                    const Text("Height", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    Text(widget.people.height.toString(), style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                                    const Spacer(),
                                  ],
                                ),

                              ),

                              const Spacer(),

                              Container(
                                width: 110,
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                                  color: Colors.blueAccent.withOpacity(0.1),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    const Spacer(),
                                    const Text("Mass", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    Text(widget.people.mass.toString(), style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                                    const Spacer(),
                                  ],
                                ),
                              ),

                              const Spacer(),

                              Container(
                                width: 110,
                                height: 60,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                                  color: Colors.blueAccent.withOpacity(0.1),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    const Spacer(),
                                    const Text("Skin Color", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    Text(widget.people.skinColor.toString(), style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                                    const Spacer(),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 24,
                          ),

                          const Text("Details", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),),
                          const SizedBox(
                            height: 8,
                          ),
                          Text("Created : " + widget.people.created.toString(), style: const TextStyle(fontSize: 13, fontWeight: FontWeight.w300),),
                          const SizedBox(
                            height: 8,
                          ),
                          Text("Edited : " + widget.people.edited.toString(), style: const TextStyle(fontSize: 13, fontWeight: FontWeight.w300),),

                          const SizedBox(
                            height: 250,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          bottomNavigationBar: BottomAppBar(
            child: Container(
              height: 70,
              padding: const EdgeInsets.only(left: 16, right: 16),
              width: MediaQuery.of(context).copyWith().size.width,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child:MyMainButton(
                      buttonColor: widget.status == "Favorite" ? Colors.redAccent : Colors.blue,
                      onPress: () {
                        if(widget.status == "Favorite"){
                          fBloc.add(RemoveFavorite(people: widget.people, context: context));
                        }
                        else{
                          fBloc.add(AddFavorite(context: context, people: widget.people));
                        }
                      },
                      buttonText: widget.status == "Favorite" ? "Remove From Favorite" : "Add to Favorite",
                    ),
                  ),
                ],
              ),
            ),
            color: Colors.white,
          ),
        ),

    );
  }
}
