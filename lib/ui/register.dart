import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/bloc/auth_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_button_with_icon.dart';
import 'package:majoo_jonatan/custom_widget/my_horizontal_divider.dart';
import 'package:majoo_jonatan/custom_widget/my_main_button.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

final TextEditingController usernameControl = TextEditingController();
final TextEditingController passwordControl = TextEditingController();
final TextEditingController rePasswordControl = TextEditingController();
final TextEditingController emailControl = TextEditingController();

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    AuthBloc aBloc = BlocProvider.of<AuthBloc>(context);
    Size size = MediaQuery.of(context).size;
    return BlocProvider<AuthBloc>(
      create: (BuildContext context) {
        return aBloc;
      },
      child:Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  left: 24, right: 24, top: size.height * 0.05, bottom: 24),
              width: size.width,
              height: size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.blueAccent),
                    alignment: Alignment.center,
                    child: const Icon(
                      Ionicons.person,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  const Text(
                    "Welcome",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.black54),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  const Text(
                    "Find your friend",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Colors.black54),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  Row(
                    children: const <Widget>[
                      Text(
                        "Register",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: Colors.black54),
                      ),
                      Spacer(),
                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: const <Widget>[
                      Text(
                        "Enter your identity to create an account",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.black54),
                      ),
                      Spacer(),
                    ],
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your user name ';
                      } else {}
                      return null;
                    },
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: usernameControl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'User Name',
                      hintText: 'example : Your User Name',
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your email';
                      } else {}
                      return null;
                    },
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: emailControl,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'Email',
                      hintText: 'example : user@gmail.com',
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your password';
                      } else {}
                      return null;
                    },
                    textAlignVertical: TextAlignVertical.center,
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: passwordControl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(
                          1 == 1
                              ? Ionicons.eye_outline
                              : Ionicons.eye_off_outline,
                          color: Colors.grey,
                        ),
                        onPressed: () {},
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'Password',
                      hintText: 'xxxxxxxxx',
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your Retype password';
                      } else {}
                      return null;
                    },
                    textAlignVertical: TextAlignVertical.center,
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: rePasswordControl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(
                          1 == 1
                              ? Ionicons.eye_outline
                              : Ionicons.eye_off_outline,
                          color: Colors.grey,
                        ),
                        onPressed: () {},
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'Retype Password',
                      hintText: 'xxxxxxxxx',
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  MyMainButton(
                    onPress: () {
                      aBloc.add(RegisterUser(username: usernameControl.text.toString(), email: emailControl.text.toString(), password: passwordControl.text.toString(), context: context));
                    },
                    buttonText: "Register",
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: const <Widget>[
                      Expanded(
                        flex: 1,
                        child: MyHorizontalDivider(),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "Register With",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.grey),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        flex: 1,
                        child: MyHorizontalDivider(),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: const <Widget>[
                      Expanded(
                        flex: 1,
                        child: MyButtonWithIcon(
                          buttonColor: Colors.white,
                          buttonText: 'Facebook',
                          buttonTextColor: Colors.grey,
                          buttonIcon: Icon(
                            Ionicons.logo_facebook,
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        flex: 1,
                        child: MyButtonWithIcon(
                          buttonColor: Colors.white,
                          buttonText: 'Google',
                          buttonTextColor: Colors.grey,
                          buttonIcon: Icon(
                            Ionicons.logo_google,
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  GestureDetector(
                    onTap: () {
                      aBloc.add(NavigationAuth(context: context, page: "Login"));
                    },
                    child: const Text(
                      "Already have an account ?",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Colors.blueAccent),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      ),
    );
  }
}
