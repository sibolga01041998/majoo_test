import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_jonatan/bloc/people_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_empty_state.dart';
import 'package:majoo_jonatan/custom_widget/my_loading_people_card_list.dart';
import 'package:majoo_jonatan/custom_widget/my_people_card_list.dart';
import 'package:majoo_jonatan/utils/my_text.dart';


class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider<PeopleBloc>(
        create: (context) => PeopleBloc()..add(GetPeopleAPIEvent()),
        child: NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (scrollInfo is ScrollEndNotification &&
                scrollInfo.metrics.extentAfter == 0) {
              return true;
            }
            else{
            }
            return false;
          },
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                toolbarHeight: 80,
                backgroundColor: Colors.white,
                floating: true,
                pinned: false,
                snap: false,
                automaticallyImplyLeading: false,
                title: Column(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child:
                          Text(
                            "Home",
                            style: MyText.title(context)
                                .copyWith(
                                color: Colors.black87,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 10,
                      ),
                      child: IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                highlightColor: Colors.black26,
                                onTap: () {
                                },
                                child:
                                Container(
                                  color: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    children: const <Widget>[
                                      Icon(
                                        Icons.filter_alt_sharp,
                                        color: Colors.grey,
                                        size: 18,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "Filter",
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const VerticalDivider(
                              width: 1,
                              thickness: 1,
                              color: Colors.grey,
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: const <Widget>[
                                  Icon(
                                    Icons.sort,
                                    color: Colors.grey,
                                    size: 18,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "Sort",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                            const VerticalDivider(
                              width: 1,
                              thickness: 1,
                              color: Colors.grey,
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: const <Widget>[
                                  Icon(
                                    Icons.menu,
                                    color: Colors.grey,
                                    size: 18,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "Tools",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            BlocBuilder<PeopleBloc, PeopleState>(
              builder: (BuildContext context, state){
                if(state is PeopleState){
                  if(state.people == null){
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                            (context, index) => const MyLoadingPeopleCardList(),
                        childCount: 8,
                      ),
                    );
                  }
                  else{
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                            (context, index) => (index == state.people!.length) ? state is LoadingUpdatePeople
                            ?  const MyLoadingPeopleCardList()
                            :
                        const SizedBox() : MyPeopleCardList(
                            people: state.people![index],
                            status: "Home",),
                        childCount: state.people!.length + 1,
                      ),
                    );
                  }
                }
                else if(state is LoadingPeople){
                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                          (context, index) => const MyLoadingPeopleCardList(),
                      childCount: 8,
                    ),
                  );
                }
                else{
                  return SliverPadding(
                    padding: const EdgeInsets.all(0),
                    sliver: SliverToBoxAdapter(
                      child: Container(
                        color: Colors.white,
                        width: double.infinity,
                        height: size.height * 0.8,
                        child: const Center(
                          child: MyEmptyState(
                            title: "Data tidak ditemukan",
                            content:
                            "List of People Kosong..",
                          ),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
            ],
          ),
        ),
      ),
      );
  }}

