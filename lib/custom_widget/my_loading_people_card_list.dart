import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majoo_jonatan/utils/img.dart';
import 'package:majoo_jonatan/utils/my_text.dart';
import 'package:shimmer/shimmer.dart';

class MyLoadingPeopleCardList extends StatefulWidget {

  const MyLoadingPeopleCardList({
    Key? key,
  }) : super(key: key);

  @override
  _MyLoadingPeopleCardListState createState() => _MyLoadingPeopleCardListState();
}

class _MyLoadingPeopleCardListState extends State<MyLoadingPeopleCardList> {

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {

      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 1,
                    color: Colors.black87.withOpacity(0.4),
                    offset: const Offset(1, 1),
                    blurRadius: 8,
                  )
                ],
              ),
              child:  Shimmer.fromColors(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 70,
                      width: double.infinity,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    topRight: Radius.circular(12))),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12)),
                              child: Image.asset(Img.get("background.webp"),
                                  fit: BoxFit.cover,
                                  height: 180,
                                  width: double.infinity),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.black38.withOpacity(0.3),
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    topRight: Radius.circular(12))),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(12),
                                      child: Image.asset(Img.get("avatar.webp"),
                                          fit: BoxFit.cover,
                                          height: 50,
                                          width: 50),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              SizedBox(
                                                height: 15,
                                                width: 50,
                                                child: Container(
                                                  color: Colors.red,
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              SizedBox(
                                                height: 15,
                                                width: 50,
                                                child: Container(
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              SizedBox(
                                                height: 15,
                                                width: 50,
                                                child: Container(
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const Spacer(),

                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Height',
                                  style: MyText.caption(context).copyWith(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.normal),
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                SizedBox(
                                  height: 15,
                                  width: 50,
                                  child: Container(
                                    color: Colors.red,
                                  ),
                                ),

                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Mass',
                                  style: MyText.caption(context).copyWith(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.normal),
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                SizedBox(
                                  height: 15,
                                  width: 50,
                                  child: Container(
                                    color: Colors.red,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'Hair Color',
                                      style: MyText.caption(context).copyWith(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    ),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    SizedBox(
                                      height: 15,
                                      width: 50,
                                      child: Container(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ],
                                ),

                              ],
                            ),
                          ),

                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[

                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'Skin Color',
                                      style: MyText.caption(context).copyWith(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    ),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    SizedBox(
                                      height: 15,
                                      width: 50,
                                      child: Container(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[200]!),
            ),
          ],
        ),
      ),
    );
  }
}
