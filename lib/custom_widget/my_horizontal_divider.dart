import 'package:flutter/material.dart';

class MyHorizontalDivider extends StatefulWidget {

  final Color? color;
  final double? marginTop;
  final double? marginBot;
  final double? marginLeft;
  final double? marginRight;

  const MyHorizontalDivider({
    this.color,
    Key? key,
    this.marginTop,
    this.marginBot,
    this.marginLeft,
    this.marginRight,
  }) : super(key: key);

  @override
  _MyHorizontalDividerState createState() => _MyHorizontalDividerState();
}

class _MyHorizontalDividerState extends State<MyHorizontalDivider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: widget.marginLeft ?? 0 ,
          right: widget.marginRight ?? 0 ,
          top: widget.marginTop ?? 0 ,
          bottom: widget.marginBot ?? 0 ),
      child: Divider(
            height: 1,
            thickness: 1,
            color: widget.color ?? Colors.grey.withOpacity(0.5),
          ),
    );
  }
}
