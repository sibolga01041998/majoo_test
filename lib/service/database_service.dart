import 'package:majoo_jonatan/model/people_model.dart';
import 'package:majoo_jonatan/model/user_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseService {
  static final DatabaseService instance = DatabaseService._init();

  static Database? _database;
  var userList = <UserModel>[];
  var peopleList = <PeopleModel>[];
  var favoriteList = <PeopleModel>[];

  DatabaseService._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('majoo_test.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    await db.execute(
      '''CREATE TABLE user( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT, email TEXT, password TEXT )''',
    );
    //---------------------------
    await db.execute(
      '''CREATE TABLE people( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, height TEXT, mass TEXT, hair_color TEXT, skin_color TEXT, eye_color TEXT, birth_year TEXT, gender TEXT, homeworld TEXT, films TEXT, species TEXT, vehicles TEXT, starships  TEXT, created TEXT, edited TEXT, url TEXT )''',
    );
    //---------------------------
    await db.execute(
      '''CREATE TABLE favorite( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,name TEXT, height TEXT, mass TEXT, hair_color TEXT, skin_color TEXT, eye_color TEXT, birth_year TEXT, gender TEXT, homeworld TEXT, films TEXT, species TEXT, vehicles TEXT, starships TEXT, created TEXT, edited TEXT, url TEXT )''',
    );
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  //Delete----------------------------------------------------------------------
  Future<void> clearDB() async {
    final db = await database;
    db.delete("user");
    db.delete("people");
    db.delete("favorite");
  }

  //Insert----------------------------------------------------------------------
  Future insertUser(UserModel user) async {
    final db = await database;
    var res = await db.rawQuery("SELECT * FROM user WHERE username = '${user.username.toString()}'");
    if (res.isNotEmpty) {
      print("Gagal");
      return null;
    }
    else{
      print("True");
      return await db.insert('user', user.toJson(), conflictAlgorithm: ConflictAlgorithm.fail,);
    }
  }

  Future insertPeople(PeopleModel people) async {
    final db = await database;
    return await db.insert(
      'people',
      people.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future insertFavorite(PeopleModel favorite) async {
    final db = await database;
    return await db.insert(
      'favorite', favorite.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  //Select----------------------------------------------------------------------
  Future<List<UserModel>> getUser() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('user');
    for (Map user in maps) {
      userList.add(UserModel().fromJson(user));
    }
    return userList;
  }

  Future<List<PeopleModel>> getPeople() async {
    final db = await database;
    peopleList.clear();
    final List<Map<String, dynamic>> maps = await db.query('people');
    for (Map people in maps) {
      peopleList.add(PeopleModel().fromJson(people));
    }
    return peopleList;
  }

  Future<List<PeopleModel>> getFavorite() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('favorite');
    favoriteList.clear();
    for (Map favorite in maps) {
      favoriteList.add(PeopleModel().fromJson(favorite));
      print(favoriteList);
    }
    return favoriteList;
  }

  //Login----------------------------------------------------------------------
  Future getLogin(String username, String password) async {
    var db = await database;
    var res = await db.rawQuery("SELECT * FROM user WHERE username = '$username' and password = '$password'");
    if (res.isNotEmpty) {
      print(res.first.toString());
      return res;
    }
    else{
      print("OKE : ${res.first.toString()}");
      return null;
    }
  }


  //Update----------------------------------------------------------------------
  Future<void> updateUser(row) async {
    final db = await database;
    await db.update(
      'user',
      row,
      where: 'id = ?',
      whereArgs: [row['id']],
    );
  }

  //Delete----------------------------------------------------------------------
  Future<int> removeFavorite(PeopleModel people) async {
    final db = await database;

    var result = await db.delete(
      'favorite',
      where: 'name = ?',
      whereArgs: [people.name.toString()],
    );
    return result;
  }
}
