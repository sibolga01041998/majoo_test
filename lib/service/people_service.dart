import 'dart:convert';
import 'package:http/http.dart' as http;

class PeopleService{
  static var client = http.Client();
  var body = {};

  Future<Map<dynamic,dynamic>> getPeopleList(url) async {
    var head = <String, String>{};
    head['Content-Type'] = 'application/json';
    var response = await client.get(Uri.parse(url),headers: head);
    if(response.statusCode == 200){
      body = jsonDecode(response.body);
    }
    else {
    }
    return body;
  }

}