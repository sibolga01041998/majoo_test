import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/bloc/profile_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_horizontal_divider.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: BlocProvider<ProfileBloc>(
        create: (context) => ProfileBloc()..add(GetProfileEvent()),
    child: CustomScrollView(
          slivers: [
            SliverAppBar(
              expandedHeight: 300.0,
              floating: true,
              elevation: 0,
              backgroundColor: Colors.transparent,
              automaticallyImplyLeading: false,
              actionsIconTheme: const IconThemeData(opacity: 0.0),
              flexibleSpace: Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: ClipRRect(
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(24),
                            bottomRight: Radius.circular(24)),
                        child: CachedNetworkImage(
                          placeholder: (context, url) => const Center(
                            child: SizedBox(
                              width: 35.0,
                              height: 35.0,
                              child: CupertinoActivityIndicator(),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(
                            Icons.error,
                            color: Colors.black87.withOpacity(0.5),
                          ),
                          fadeInDuration: const Duration(milliseconds: 0),
                          fadeOutDuration: const Duration(milliseconds: 0),
                          imageUrl:
                          'https://images.unsplash.com/photo-1560743173-567a3b5658b1?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MXwxfDB8MXxhbGx8fHx8fHx8fA&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit',
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: 320.0,
                        )),
                  ),
                  SizedBox(
                    width: size.width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(40),
                          ),
                          child: Image.network(
                            "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                            fit: BoxFit.cover,
                            width: 80,
                            height: 80,
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        const Text(
                          "Jonatan Prim",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        const Text(
                          "jonatanhutabarat10@gmail.com",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 16, right: 16),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: 110,
                                height: 60,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: const <Widget>[
                                    Spacer(),
                                    Text(
                                      "Text 1",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w300),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      "3",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ),
                              const Spacer(),
                              Container(
                                width: 110,
                                height: 60,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: const <Widget>[
                                    Spacer(),
                                    Text(
                                      "Text 2",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w300),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      "2",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ),
                              const Spacer(),
                              Container(
                                width: 110,
                                height: 60,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: const <Widget>[
                                    Spacer(),
                                    Text(
                                      "Text 3",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w300),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      "1",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 16, right: 15, top: 12),
              sliver: SliverToBoxAdapter(
                child: Wrap(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Text(
                          "Information",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.person, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Hair Color",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "Black",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.person, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Skin Color",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "Brown",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.person, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Eye Color",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "Black",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.person, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Birth Date",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "19BBY",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),

                            ],
                          ),
                        ),

                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        const Text(
                          "Settings",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.language_outline, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Languange",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "English",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              Icon(Ionicons.chevron_forward, color: Colors.grey.withOpacity(0.5), size: 20,),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.lock_closed_outline, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Change Password",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              Icon(Ionicons.chevron_forward, color: Colors.grey.withOpacity(0.5), size: 20,),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.notifications_outline, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Notification",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              const Text(
                                "Enable",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300, color: Colors.blueAccent),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              Icon(Ionicons.chevron_forward, color: Colors.grey.withOpacity(0.5), size: 20,),
                            ],
                          ),
                        ),
                        MyHorizontalDivider(
                          color: Colors.black87.withOpacity(0.1),
                        ),
                        Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(Ionicons.log_out_outline, color: Colors.grey.withOpacity(0.5), size: 20,),
                              const SizedBox(
                                width: 12,
                              ),
                              const Text(
                                "Logout",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                              const Spacer(),
                              Icon(Ionicons.chevron_forward, color: Colors.grey.withOpacity(0.5), size: 20,),
                            ],
                          ),
                        ),

                        const SizedBox(
                          height: 500,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      ),
    );
  }
}
