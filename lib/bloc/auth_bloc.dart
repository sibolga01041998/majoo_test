import 'dart:convert';
import 'dart:developer';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_jonatan/model/user_model.dart';
import 'package:majoo_jonatan/service/database_service.dart';
import 'package:majoo_jonatan/ui/login.dart';
import 'package:majoo_jonatan/ui/register.dart';
import 'package:majoo_jonatan/ui/root.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {

 AuthBloc() : super(const AuthState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if(event is RegisterUser){
      log("Starttttt");
      try {
        yield LoadingAuth();
        log("${event.username}");
        var user = UserModel(username: event.username,
            password: event.password,
            email: event.email);
        var db = DatabaseService.instance;
        var result = await db.insertUser(user);
        log("RESULT : $result");
        if(result == null){

        }
        else{
          Navigator.push(event.context,
              MaterialPageRoute(builder: (context) =>
                  BlocProvider<AuthBloc>(
                      create: (context) => AuthBloc(),
                      child: const Login()),
              ));
        }
      }
      catch(e){
        log(e.toString());
        yield FailureAuth(e.toString());
      }
    }
    else if(event is LoginUser){
      try{
        yield LoadingAuth();
        var db = DatabaseService.instance;
        var result = await db.getLogin(event.username, event.password);
        log("TEST : ${result.toString()}");
        if(result != null){
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("DataLogin", jsonEncode(result));
          Navigator.pushReplacement(event.context,
              MaterialPageRoute(builder: (context) => BlocProvider<AuthBloc>(
                  create:(context) => AuthBloc(),
                  child: const Root()),
              ));
        }
        else{
        }
      }
      catch(e){
        yield FailureAuth(e.toString());
      }
    }

    else if(event is NavigationAuth){
      try{
        yield LoadingAuth();
         if(event.page == "Login"){
           Navigator.push(event.context,
             MaterialPageRoute(builder: (context) => BlocProvider<AuthBloc>(
                 create:(context) => AuthBloc(),
                 child: const Login()),
           ));
         }
         else{
           Navigator.push(event.context,
             MaterialPageRoute(builder: (context) => BlocProvider<AuthBloc>(
                 create:(context) => AuthBloc(),
                 child: const Register()),
           ));
         }
      }
      catch(e){
        yield FailureAuth(e.toString());
      }
    }



  }

}

//Event
abstract class AuthEvent {}

class RegisterUser extends AuthEvent{
  BuildContext context;
  String? username;
  String? password;
  String? email;

  RegisterUser({
    this.username, this.password, this.email, required this.context
});
}

class LoginUser extends AuthEvent{
  BuildContext context;
  String username;
  String password;

  LoginUser({
    required this.username, required this.password, required this.context
  });
}

class NavigationAuth extends AuthEvent{
  BuildContext context;
  String page;
  NavigationAuth({required this.context, required this.page});
}


//State
class AuthState {
  final UserModel? user;
  const AuthState({this.user});
  factory AuthState.initial() => const AuthState();
}

class FailureAuth extends AuthState {
  final String error;
  FailureAuth(this.error);
}

class LoadingAuth extends AuthState{}