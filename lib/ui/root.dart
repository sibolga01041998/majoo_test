import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:majoo_jonatan/custom_widget/my_bottom_navigation_bar.dart';

class Root extends StatefulWidget {

  const Root({Key? key}) : super(key: key);

  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.white
    ));
    return const MyBottomNavigationBar();
  }
}