import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_jonatan/bloc/favorite_bloc.dart';
import 'package:majoo_jonatan/model/people_model.dart';
import 'package:majoo_jonatan/ui/detail_people.dart';
import 'package:majoo_jonatan/utils/img.dart';
import 'package:majoo_jonatan/utils/my_text.dart';

class MyPeopleCardList extends StatefulWidget {
  final PeopleModel people;
  String status;

  MyPeopleCardList({
    required this.people,
    required this.status,
    Key? key,
  }) : super(key: key);

  @override
  _MyPeopleCardListState createState() => _MyPeopleCardListState();
}

class _MyPeopleCardListState extends State<MyPeopleCardList> {

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) =>
                BlocProvider<FavoriteBloc>(
                    create: (context) => FavoriteBloc(),
                    child: DetailPeople(people: widget.people, status: widget.status,)),
            ));
      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    spreadRadius: 1,
                    color: Colors.black87.withOpacity(0.2),
                    offset: const Offset(1, 1),
                    blurRadius: 6,
                  )
                ],
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 70,
                    width: double.infinity,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child: ClipRRect(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(12),
                                topRight: Radius.circular(12)),
                            child: Image.asset(Img.get("background.webp"),
                                  fit: BoxFit.cover,
                                  height: 180,
                                  width: double.infinity),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              color: Colors.black38.withOpacity(0.3),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(12),
                                    child: Image.asset(Img.get("avatar.webp"),
                                        fit: BoxFit.cover,
                                        height: 50,
                                        width: 50),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text(widget.people.name.toString(),
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 12),
                                            ),
                                            const SizedBox(
                                              width: 8,
                                            ),
                                            Text(widget.people.birthYear.toString(),
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 12),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text(widget.people.gender.toString(),
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w300,
                                                  fontSize: 10),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const Spacer(),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Height',
                                style: MyText.caption(context).copyWith(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.normal),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Text(widget.people.height.toString(),
                                style: MyText.caption(context).copyWith(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),

                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Mass',
                                style: MyText.caption(context).copyWith(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.normal),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Text(widget.people.mass.toString(),
                                style: MyText.caption(context).copyWith(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'Hair Color',
                                    style: MyText.caption(context).copyWith(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(widget.people.hairColor.toString(),
                                    style: MyText.caption(context).copyWith(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),

                            ],
                          ),
                        ),

                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[

                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'Skin Color',
                                    style: MyText.caption(context).copyWith(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(widget.people.skinColor.toString(),
                                    style: MyText.caption(context).copyWith(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
