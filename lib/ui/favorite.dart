import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_jonatan/bloc/favorite_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_empty_state.dart';
import 'package:majoo_jonatan/custom_widget/my_loading_people_card_list.dart';
import 'package:majoo_jonatan/custom_widget/my_people_card_list.dart';
import 'package:majoo_jonatan/utils/my_text.dart';

class Favorite extends StatefulWidget {
  const Favorite({Key? key}) : super(key: key);

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocProvider<FavoriteBloc>(
        create: (context) => FavoriteBloc()..add(GetFavoriteEvent()),
        child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                toolbarHeight: 80,
                backgroundColor: Colors.white,
                floating: true,
                pinned: false,
                snap: false,
                automaticallyImplyLeading: false,
                title: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child:
                          Text(
                            "Favorite",
                            style: MyText.title(context)
                                .copyWith(
                                color: Colors.black87,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                ),
              BlocBuilder<FavoriteBloc, FavoriteState>(
                builder: (BuildContext context, state){
                  if(state is FavoriteState){
                    if(state.favorite == null){
                      return SliverList(
                        delegate: SliverChildBuilderDelegate(
                              (context, index) => const MyLoadingPeopleCardList(),
                          childCount: 8,
                        ),
                      );
                    }
                    else{
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                            (context, index) => (index == state.favorite!.length) ? state is LoadingUpdateFavorite
                            ?  const MyLoadingPeopleCardList()
                            :
                        const SizedBox() : MyPeopleCardList(
                            people: state.favorite![index],
                            status: "Favorite",),
                        childCount: state.favorite!.length + 1,
                      ),
                    );
                  }
                  }
                  else if(state is LoadingFavorite){
                    return SliverGrid(
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200.0,
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        childAspectRatio: 4.0,
                      ),
                      delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return Container(
                            alignment: Alignment.center,
                            color: Colors.teal[100 * (index % 9)],
                            child: Text('grid item $index'),
                          );
                        },
                        childCount: 20,
                      ),
                    );
                  }
                  else{
                    return SliverPadding(
                      padding: const EdgeInsets.all(0),
                      sliver: SliverToBoxAdapter(
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,
                          height: size.height * 0.8,
                          child: const Center(
                            child: MyEmptyState(
                              title: "Data tidak ditemukan",
                              content:
                              "List of Favorite Kosong..",
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                },
              ),
            ],
        ),
      ),    );
  }
}
