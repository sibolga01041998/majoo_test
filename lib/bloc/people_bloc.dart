import 'package:bloc/bloc.dart';
import 'package:majoo_jonatan/model/people_model.dart';
import 'package:majoo_jonatan/service/database_service.dart';
import 'package:majoo_jonatan/service/people_service.dart';
import 'package:majoo_jonatan/utils/my_data.dart';



class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  var peopleList = <PeopleModel>[];
  var peopleListDB = <PeopleModel>[];
  var url = MyData().initialUrl;

  PeopleBloc() : super(const PeopleState());

  @override
  Stream<PeopleState> mapEventToState(PeopleEvent event) async* {
    if(event is GetPeopleAPIEvent){
      try{
        yield LoadingPeople();
        final peopleData = await PeopleService().getPeopleList(url.toString());
        if(peopleData != {}){
          for (Map people in peopleData["results"]) {
            peopleList.add(PeopleModel().fromJson(people));
            savePeopleList(PeopleModel().fromJson(people));
          }
          url = peopleData['next'].toString();
          var data = await DatabaseService.instance.getPeople();
          yield PeopleState(people: data);
        }
        else{
          yield FailurePeople("Something Error");
        }

      }
      catch(e){
        yield FailurePeople(e.toString());
      }
    }

  }

  Future savePeopleList(PeopleModel people) async {
    print("INSERT : $people");
    var db = DatabaseService.instance;
    await db.insertPeople(people);
  }

}

//Event
abstract class PeopleEvent {}

class GetPeopleAPIEvent extends PeopleEvent{
  GetPeopleAPIEvent();
}

class UpdatePeopleAPIEvent extends PeopleEvent{
  UpdatePeopleAPIEvent();
}

//State
class PeopleState {
  final List<PeopleModel>? people;
  const PeopleState({this.people});
  factory PeopleState.initial() => const PeopleState();
}

class FailurePeople extends PeopleState {
  final String error;
  FailurePeople(this.error);
}

class LoadingPeople extends PeopleState{}

class LoadingUpdatePeople extends PeopleState{}