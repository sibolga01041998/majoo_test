import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majoo_jonatan/model/people_model.dart';
import 'package:majoo_jonatan/service/database_service.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  var favoriteList = <PeopleModel>[];

  FavoriteBloc() : super(const FavoriteState());

  @override
  Stream<FavoriteState> mapEventToState(FavoriteEvent event) async* {
    if(event is GetFavoriteEvent){
      try{
        yield LoadingFavorite();
        print("Get Favorite");
        var data = await DatabaseService.instance.getFavorite();
        favoriteList = data;
        print(data.length);
        print(favoriteList.length);
        yield FavoriteState(favorite: favoriteList);

      }
      catch(e){
        yield FailureFavorite(e.toString());
      }
    }
    if(event is AddFavorite){
      try {
        yield LoadingFavorite();
        var db = DatabaseService.instance;
        var result = await db.insertFavorite(event.people);
        if(result == null){
          createDialog(event.context, "Gagal", "Gagal Menambahkan ke Favorite");
        }
        else{
          createDialog(event.context, "Sukses", "Berhasil Menambahkan ke Favorite");
        }
      }
      catch(e){
        yield FailureFavorite(e.toString());
      }
    }

    if(event is RemoveFavorite){
      try {
        yield LoadingFavorite();
        var db = DatabaseService.instance;
        var result = await db.removeFavorite(event.people);
        print(result);
        if(result.toString() != "1"){
          createDialog(event.context, "Gagal", "Gagal Menghapus dari Favorite");
        }
        else{
          createDialog(event.context, "Sukses", "Berhasil Menghapus dari Favorite");
        }
      }
      catch(e){
        yield FailureFavorite(e.toString());
      }
    }


  }

  Future<void> createDialog(context, title, message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(title.toString()),
          content: Text(message.toString()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: const Text("OK"),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

}

//Event
abstract class FavoriteEvent {}

class AddFavorite extends FavoriteEvent{
  BuildContext context;
  PeopleModel people;

  AddFavorite({
    required this.people, required this.context
  });
}

class RemoveFavorite extends FavoriteEvent{
  BuildContext context;
  PeopleModel people;

  RemoveFavorite({
    required this.people, required this.context
  });
}

class GetFavoriteEvent extends FavoriteEvent{
  GetFavoriteEvent();
}

class UpdateFavoriteEvent extends FavoriteEvent{
  UpdateFavoriteEvent();
}

//State
class FavoriteState {
  final List<PeopleModel>? favorite;
  const FavoriteState({this.favorite});
  factory FavoriteState.initial() => const FavoriteState();
}

class FailureFavorite extends FavoriteState {
  final String error;
  FailureFavorite(this.error);
}

class LoadingFavorite extends FavoriteState{}

class LoadingUpdateFavorite extends FavoriteState{}