import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/bloc/auth_bloc.dart';
import 'package:majoo_jonatan/custom_widget/my_button_with_icon.dart';
import 'package:majoo_jonatan/custom_widget/my_horizontal_divider.dart';
import 'package:majoo_jonatan/custom_widget/my_main_button.dart';


class Login extends StatefulWidget {

  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

final TextEditingController usernameControl = TextEditingController();
final TextEditingController passwordControl = TextEditingController();


class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    AuthBloc aBloc = BlocProvider.of<AuthBloc>(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  left: 24, right: 24, top: size.height * 0.1, bottom: 24),
              width: size.width,
              height: size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.blueAccent),
                    alignment: Alignment.center,
                    child: const Icon(
                      Ionicons.person,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  const Text(
                    "Welcome",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.black54),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  const Text(
                    "Find your friend",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Colors.black54),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  Row(
                    children: const <Widget>[
                      Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: Colors.black54),
                      ),
                      Spacer(),
                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: const <Widget>[
                      Text(
                        "Enter your username and password",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.black54),
                      ),
                      Spacer(),
                    ],
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your username';
                      } else {}
                      return null;
                    },
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: usernameControl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'User Name',
                      hintText: 'contoh : username',
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter your password';
                      } else {}
                      return null;
                    },
                    textAlignVertical: TextAlignVertical.center,
                    cursorWidth: 1,
                    autocorrect: true,
                    autofocus: false,
                    controller: passwordControl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(
                          1 ==  1
                              ? Ionicons.eye_outline
                              : Ionicons.eye_off_outline,
                          color: Colors.grey,
                        ),
                        onPressed: () {

                        },
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueAccent.withOpacity(0.5),
                            width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.withOpacity(0.5), width: 1.0),
                      ),
                      labelText: 'Password',
                      hintText: 'xxxxxxxxx',
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: const <Widget>[
                      Spacer(),
                      Text(
                        "Forgot Password ?",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.blueAccent),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  MyMainButton(
                    onPress: () {
                      aBloc.add(LoginUser(username: usernameControl.text.toString(), password: passwordControl.text.toString(), context: context));
                    },
                    buttonText: "Login",
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: const <Widget>[
                      Expanded(
                        flex: 1,
                        child: MyHorizontalDivider(),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "Login With",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.grey),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        flex: 1,
                        child: MyHorizontalDivider(),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: const <Widget>[
                      Expanded(
                        flex: 1,
                        child: MyButtonWithIcon(
                          buttonColor: Colors.white,
                          buttonText: 'Facebook',
                          buttonTextColor: Colors.grey,
                          buttonIcon: Icon(
                            Ionicons.logo_facebook,
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        flex: 1,
                        child: MyButtonWithIcon(
                          buttonColor: Colors.white,
                          buttonText: 'Google',
                          buttonTextColor: Colors.grey,
                          buttonIcon: Icon(
                            Ionicons.logo_google,
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  GestureDetector(
                    onTap: (){
                      aBloc.add(NavigationAuth(context: context, page: "Register"));
                    },
                    child: const Text(
                      "Don't have an account ?",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Colors.blueAccent),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
  ),
    );
  }
}
