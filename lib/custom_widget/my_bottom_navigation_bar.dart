import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/ui/favorite.dart';
import 'package:majoo_jonatan/ui/home.dart';
import 'package:majoo_jonatan/ui/profile.dart';


class MyBottomNavigationBar extends StatefulWidget {

  const MyBottomNavigationBar({Key? key}) : super(key: key);

  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {

  int _currentIndex = 1;
  final List<Widget> _children = [
    const Favorite(),
    const Home(),
    const Profile(),
  ];

  void onTappedBar(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTappedBar,
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.grey.withOpacity(0.5),
        selectedFontSize: 14,
        unselectedFontSize: 12,
        unselectedLabelStyle: const TextStyle(fontSize: 10),
        selectedLabelStyle: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.black54),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Ionicons.heart),
            label: 'Favorite',),

          BottomNavigationBarItem(
            icon: Icon(Ionicons.home),
            label: 'Home',),

          BottomNavigationBarItem(
              icon: Icon(Ionicons.person),
              label: 'Profile'),
        ],
      ),
    );
  }
}
