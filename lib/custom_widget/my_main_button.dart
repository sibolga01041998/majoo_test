import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyMainButton extends StatefulWidget {
  final String buttonText;
  final double? width;
  final double? height;
  final Color? buttonColor;
  final Color? buttonTextColor;
  final void Function()? onPress;
  
  const MyMainButton({
    Key? key,
    this.onPress,
    required this.buttonText,
    this.width,
    this.height,
    this.buttonColor,
    this.buttonTextColor
  }) : super(key: key);

  @override
  _MyMainButtonState createState() => _MyMainButtonState();
}

class _MyMainButtonState extends State<MyMainButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width ?? double.infinity,
      height: widget.height ?? 50,
      child: CupertinoButton(
        color: widget.buttonColor ?? Colors.blueAccent,
        onPressed: widget.onPress,
        child: Text(widget.buttonText, style: TextStyle(color: widget.buttonTextColor, fontSize: 14, fontWeight: FontWeight.w500), ),
      ),
    );
  }
}
