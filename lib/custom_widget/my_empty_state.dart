import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:majoo_jonatan/utils/img.dart';

class MyEmptyState extends StatefulWidget {
  final String? title;
  final String? content;
  final String? image;
  final int? imageType;
  final String? actionTitle;
  final bool? withAction;
  final bool? withNavigation;
  final String? navigationTitle1;
  final String? navigationTitle2;
  final void Function()? navigationAct1;
  final void Function()? navigationAct2;
  final void Function()? action;
  final bool? addCircle;
  final Color? colorNav1;
  final Color? colorNav2;
  final Color? colorTitleNav1;
  final Color? colorTitleNav2;
  final bool? iconNav1;
  final bool? iconNav2;

  const MyEmptyState({
    Key? key,
    this.title,
    this.content,
    this.image,
    this.imageType,
    this.withAction,
    this.action,
    this.actionTitle,
    this.withNavigation,
    this.navigationTitle1,
    this.navigationTitle2,
    this.navigationAct1,
    this.navigationAct2,
    this.addCircle,
    this.colorNav1,
    this.colorNav2,
    this.colorTitleNav1,
    this.colorTitleNav2,
    this.iconNav1,
    this.iconNav2,
  }) : super(key: key);

  @override
  _MyEmptyStateState createState() => _MyEmptyStateState();
}

class _MyEmptyStateState extends State<MyEmptyState> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Visibility(
              visible: widget.addCircle ?? true,
              child: Container(
                height: 270,
                width: 270,
                decoration: const BoxDecoration(
                    color: Colors.white, shape: BoxShape.circle),
              ),
            ),
            SizedBox(
                    height: 320,
                    width: 320,
                    child: Image.asset(
                        widget.image ?? Img.get("img_empty_state.png")),
                  )

          ],
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          width: double.infinity,
          child: Text(
            widget.title.toString(),
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16),
          width: double.infinity,
          child: Text(
            widget.content.toString(),
            style: const TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
                color: Colors.black54),
            textAlign: TextAlign.center,
          ),
        ),
        Visibility(
          visible: widget.withAction ?? false,
          child: Container(
            margin: const EdgeInsets.only(top: 16),
            height: 40,
            width: 120,
            child: OutlinedButton(
              onPressed: widget.action,
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1, color: Colors.black38),
              ),
              child: Text(
                widget.actionTitle.toString(),
                style: TextStyle(
                    color: Colors.black87.withOpacity(0.5),
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Visibility(
          visible: widget.withNavigation ?? false,
          child: Row(
            children: <Widget>[
              const Spacer(),
              Container(
                margin: const EdgeInsets.only(top: 16),
                height: 50,
                width: 140,
                color: widget.colorNav1 ?? Colors.transparent,
                child: OutlinedButton(
                  onPressed: widget.navigationAct1,
                  style: OutlinedButton.styleFrom(
                    side: const BorderSide(width: 1, color: Colors.black38),
                  ),
                  child: Row(
                    children: <Widget>[
                      const Spacer(),
                      Visibility(
                        visible: widget.iconNav1 ?? false,
                        child: const Icon(
                          Ionicons.qr_code,
                          color: Colors.white,
                        ),
                      ),

                      const SizedBox(
                        width: 12,
                      ),
                      Text(
                        widget.navigationTitle1.toString(),
                        style: TextStyle(
                            color: widget.colorTitleNav1 ?? Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 24,
              ),
              Container(
                margin: const EdgeInsets.only(top: 16),
                height: 50,
                width: 140,
                color: widget.colorNav2 ?? Colors.transparent,
                child: OutlinedButton(
                  onPressed: widget.navigationAct2,
                  style: OutlinedButton.styleFrom(
                    side: const BorderSide(width: 1, color: Colors.black38),
                  ),
                  child: Row(
                    children: <Widget>[
                      const Spacer(),
                      Visibility(
                        visible: widget.iconNav2 ?? false,
                        child: const Icon(
                          Ionicons.logo_whatsapp,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Text(
                        widget.navigationTitle2.toString(),
                        style: TextStyle(
                            color: widget.colorTitleNav2 ?? Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      ],
    );
  }
}
