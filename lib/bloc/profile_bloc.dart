import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';



class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileState());

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if(event is GetProfileEvent){
      try{
        yield LoadingProfile();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var data = prefs.getString("DataLogin");
        if(data != null){
          var profile = jsonDecode(data);
          yield ProfileState(profile: profile);
        }
        else{
          yield FailureProfile("Gagal Mendapatkan Profile");
        }
      }
      catch(e){
        yield FailureProfile(e.toString());
      }
    }
  }

}

//Event
abstract class ProfileEvent {}

class GetProfileEvent extends ProfileEvent{
  GetProfileEvent();
}

class UpdateProfileAPIEvent extends ProfileEvent{
  UpdateProfileAPIEvent();
}

//State
class ProfileState {
   dynamic profile;
  ProfileState({this.profile});
  factory ProfileState.initial() => ProfileState();
}

class FailureProfile extends ProfileState {
  final String error;
  FailureProfile(this.error);
}

class LoadingProfile extends ProfileState{}

class LoadingUpdateProfile extends ProfileState{}