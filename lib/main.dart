import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_jonatan/bloc/auth_bloc.dart';
import 'package:majoo_jonatan/ui/login.dart';
import 'package:majoo_jonatan/ui/root.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main()async{

  WidgetsFlutterBinding.ensureInitialized();
  Widget _defaultHome = BlocProvider<AuthBloc>(
      create:(context) => AuthBloc(),
      child: const Login());
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var session = prefs.getString("DataLogin");
  if(session == null){
    _defaultHome = BlocProvider<AuthBloc>(
        create:(context) => AuthBloc(),
        child: const Login());
  }
  else{
    _defaultHome = BlocProvider<AuthBloc>(
        create:(context) => AuthBloc(),
        child: const Root());
  }
  runApp(MyApp(_defaultHome));
}

class MyApp extends StatelessWidget {

  Widget? defaultHome;

  MyApp(this.defaultHome, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Majoo Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: defaultHome
    );
  }

}



