import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MyButtonWithIcon extends StatefulWidget {
  final String buttonText;
  final double? width;
  final double? height;
  final Icon buttonIcon;
  final Color? buttonColor;
  final Color? buttonTextColor;
  final void Function()? onPress;
  
  const MyButtonWithIcon({
    Key? key,
    this.onPress,
    required this.buttonText,
    this.width,
    this.height,
    this.buttonColor,
    this.buttonTextColor,
    required this.buttonIcon
  }) : super(key: key);

  @override
  _MyButtonWithIconState createState() => _MyButtonWithIconState();
}

class _MyButtonWithIconState extends State<MyButtonWithIcon> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(1),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.5),
        borderRadius: const BorderRadius.all(Radius.circular(8))
      ),
      width: widget.width ?? double.infinity,
      height: widget.height ?? 50,
      child: CupertinoButton(
        padding: EdgeInsets.zero,
        color: widget.buttonColor ?? Colors.blueAccent,
        onPressed: () {  },
        child: Row(
          children: <Widget>[
            const Spacer(),
            widget.buttonIcon,
            const SizedBox(width: 4,),
            Text(widget.buttonText, style: TextStyle(color: widget.buttonTextColor, fontSize: 14, fontWeight: FontWeight.w300), ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
