

class UserModel {
  String? id;
  String? username;
  String? email;
  String? password;

  UserModel({this.id, this.username, this.email, this.password});


  UserModel fromJson(Map<dynamic, dynamic> json) {
    return UserModel(
    id : json['id'],
    username : json['username'],
    email : json['email'],
    password : json['password']
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['email'] = email;
    data['password'] = password;
    return data;
  }
}